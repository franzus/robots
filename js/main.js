'use strict';

var app = angular.module('app', ['ngRoute', 'ngSanitize']);

app.controller('domain', ['$http', '$scope', '$location', function($http, $scope, $location){
  $scope.submit = function() {
    $http.post('./robots.php?a=getrobots', {url:$scope.url}).success(function(data) {
      $scope.result = data;
      console.log(data);
    });
  }
}]);

app.filter('checkmark', function() {
    return function(input) {
        return input ? '\u2713' : '\u2718';
    }
});

angular.bootstrap(document, ['app']);