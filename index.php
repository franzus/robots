<!DOCTYPE html>
<html>
<head>
  <title>Robots</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/main.css">

  <script src="../js/angular.min.js"></script>
</head>
<body class="page">
  <div class="page-content" ng-controller="domain">
    <div class="page-header">
      <h1 class="page-title">Analyze Robots.txt</h1>
    </div>
    <form ng-submit="submit()" class="form-horizontal">
      <div class="form-group">
        <label>Введите доменное имя</label>
        <input type="text" class="form-control" name="url" ng-model="url">
      </div>
    </form>

    <div ng-show="result">
      <div class="row">
        <div class="col-sm-3">Наличие файла robots.txt</div>
        <div class="col-sm-1">{{result.init | checkmark}}</div>
        <div class="row col-sm-4" ng-show="!result.init">
          Файл robots.txt – это простой текстовый файл, которые находится в корневом каталоге Вашего сайта. И он должен быть доступен по URL: {{result.host}}
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3">Статус ответа</div>
        <div class="col-sm-1">{{result.status == 200 | checkmark}}</div>
        <div class="row col-sm-4">{{result.status}}</div>
      </div>

      <div class="row" ng-show="result.data">
        <div class="col-sm-3">Размер файла</div>
        <div class="col-sm-1">{{!result.filesize_max | checkmark}}</div>
        <div class="row col-sm-4">
          {{result.filesize}}
        <span ng-show="result.filesize_max">
          Максимально допустимый размер файла robots.txt составляем 32 кб. Необходимо отредактировть файл robots.txt таким образом, чтобы его размер не превышал 32 Кб
        </span>
        </div>
      </div>

      <div class="row" ng-show="result.data">
        <div class="col-sm-3">Проверка директивы Host</div>
        <div class="col-sm-1">{{result.data.hosts_match | checkmark}}</div>
        <div class="row col-sm-4" ng-show="!result.data.hosts_match">
          Для того, чтобы поисковые системы знали, какая версия сайта является основных зеркалом, необходимо прописать адрес основного зеркала в директиве Host.
        </div>
      </div>

      <div class="row" ng-show="result.data && result.data.hosts_match">
        <div class="col-sm-3">Проверка дубликатов директивы Host</div>
        <div class="col-sm-1">{{result.data.hosts_match == 1 | checkmark}}</div>
        <div class="row col-sm-4" ng-show="result.data.hosts_match > 1">
          Директива Host в файле robots.txt может быть только одна, если их указано несколько, будет восприниматься только первая из них. Важно помнить, чтобы значение Host содержало корректное имя хоста или домена.
        </div>
      </div>
    </div>

      <div class="row" ng-show="result.data && result.data.hosts_match">
        <div class="col-sm-3">Проверка директивы Sitemap</div>
        <div class="col-sm-1">{{result.data.sitemap_match >= 1 | checkmark}}</div>
        <div class="row col-sm-4" ng-show="result.data.sitemap_match == 0">
          Файл Sitemap — это файл с информацией о страницах сайта, подлежащих индексированию.
        </div>
      </div>
    </div>


  </div>

  <script src="../js/ng-r.min.js"></script>
  <script src="../js/ng-sz.min.js"></script>

  <script src="./js/main.js"></script>
</body>
</html>
