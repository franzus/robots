<?php

error_reporting(E_ALL);

// Задаем тип контента для дальнейшей обработки AngularJS
header('Content-Type: application/json');

// Выбираем пост данные отправленные AngularJS
$POST = json_decode(trim(file_get_contents('php://input')), true);

// Проверяем наличие информации запрашиваемой пользователем
if (empty($POST['url'])) {
  echo json_encode(array('error' => 1));
  die();
}

// Выводи результаты обработки
$result = new robots();
echo json_encode($result->getrobots($POST['url']));

/**
 * Класс для обработки информации по файлу robots.txt
 *
 * @method array getrobots()
 * @package root
 **/
class robots
{

  /**
   * Обработка файла robots.txt
   *
   * @param $url String - ссылка введенная пользователем для получение информации
   * @return array
   **/
  public function getrobots($url)
  {
    $result = array();

    // Парсим урл
    $parsed = parse_url($url);

    // Проверяем существует ли путь, если есть, проверяем путь к файлу robots.txt
    if ($parsed['path'] === '/' || $parsed['path'] != '\/robots.txt') {
      // Если нет, присваиваем
      $url = $parsed['scheme'].'://'.$parsed['host'].'/robots.txt';
    }

    // Помещаем содержимое файла в массив, не обращая внимания на ошибки
    $data = @file($url);
    // Получаем статус ответа
    $result['status'] = substr($http_response_header[0], 9, 3);
    // Присваиваем в результирующий массив введеную ссылку
    $result['host'] = $url;

    // Проверяем заоловки ответа
    if(!empty($data) && 
        ($result['status'] == '200' || 
          ($result['status'] > 300 && $result['status'] <= 308))) {
      // Если доступен файл, инициируем
      $result['init'] = 1;

      // Получаем по заголовкам размер файла
      if (preg_match('/^Content-Length: *+\K\d++$/im', implode("\n", $http_response_header), $matches)) {
          // Обработка результата в читаемый формат
          $sz = 'BKM';
          $factor = floor((strlen((int)$matches[0]) - 1) / 3);
          $result['filesize'] = (int)$matches[0] / pow(1024, $factor) . @$sz[$factor];

          // Проверяем файл на размер, не должен привышать 32kb
          $result['filesize_max'] = (int)$matches[0] > 32768 ? 1 : 0;
      }
      // Иннициируем получение данных из файла
      $result['data'] = self::getdata($data);
    } else {
      // Отвечаем, если файла нет
      $result['init'] = 0;
    }

    // Возвращаем результат
    return $result;
  }


  /**
   * Обработка информации в файле robots.txt
   *
   * @param $data Array - содержимое файла robots.txt
   * @return array
   **/
  private function getdata($data)
  {
    $result = array();

    // Разбиваем массив на строки
    foreach($data as $line) {
      // Пропускаем пустую строку
      if(!$line = trim($line)) continue;

      // Ищем директивы Host в файле
      if(preg_match('/^\s*Host:(.*)/i', $line, $regs)) {
        $result['hosts'][] = trim($regs[1]);
      }

      // Ищем директивы Sitemap в файле
      if(preg_match('/^\s*Sitemap:(.*)/i', $line, $regs)) {
        $result['sitemap'][] = trim($regs[1]);
      }
    }

    // Считаем результаты поисков директив Host и Sitemap
    $result['hosts_match']   = count($result['hosts']);
    $result['sitemap_match'] = count($result['sitemap']);

    // Возращаем результат
    return $result;
  }
}
// END class 
